const axios = require('axios');
const slackAppBaseUrl = "http://localhost:3000/incoming/";


const sendMessage = async (nonce ,message, res) => {
    if (nonce && message)
    {
    try
    {
        let url = slackAppBaseUrl + nonce;
        console.log(url);

    let result = await axios.post(url, {message:message})
    res.status(result.status).send();
    }
    catch (error)
    {
        console.log(error);
        res.status(500).send();
    }
  }
}


module.exports = {
    sendMessage 
}


// example message objs 
// const messageExample = {
//     text: `Webhook created for <#${channelId}>:`,
//     attachments: [{
//       text: `${config.get("BASE_URL")}/incoming/${nonce}`,
//       color: '#7e1cc9',
      
//     }],
//   };

//   const messageExample1 = {
//     text: `Hi There  Let's walla!!! `,
//     attachments: [{
//       text: `http://walla.co.il}`,
//       color: '#7e1cc9',
      
//     }],
//   };




