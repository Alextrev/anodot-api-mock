const { JsonDB } = require ('node-json-db');
const { Config } = require ('node-json-db/dist/lib/JsonDBConfig')

const DB = new JsonDB(new Config("DB/channels", true, true, '/'));


const addChannel = (customerId, nonce , res) => {
    if (customerId && nonce)
    {
        DB.push(`/${nonce}`, customerId);
    }
    res.json({status:customerId && nonce ? "OK" : "Failed"});
    
}


module.exports = {addChannel}