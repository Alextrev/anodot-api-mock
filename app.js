const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const {sendMessage} = require ("./logic/messages");
const channels = require("./logic/channels");


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.get("/", (req, res) => {
    res.send("Test Anodot");
    console.log("ddddd");
});

app.post("/addchannel", (req, res) => {
    console.log(req.body);
    const {customerId, nonce} = req.body;
    console.log(customerId);
    console.log(nonce);
    channels.addChannel(customerId, nonce, res);
});


// for test from external source
app.post("/sendmessage", (req,res) => {
    console.log(req.body);
    let {nonce, message} = req.body;
    if (nonce && message )
    {
        sendMessage(nonce,message,res);
    }
    else {
        res.send("NO MESSAGE")
    }
});





  app.listen(5000, () => { console.log("server listening"); });

